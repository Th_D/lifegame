package com.company;

import java.util.ArrayList;

class Point {
    int p1, p2;

    Point(int p1, int p2) {
        this.p1 = p1;
        this.p2 = p2;
    }
}


public class Figure {
    ArrayList<Point> points = new ArrayList<>();

    Figure(Point ... points){
        for(Point p : points)
            this.points.add(p);
    }

    Figure(int fig){
        System.out.println(fig);
        switch(fig){
            case 0: //Pulsar
                this.points.add(new Point(3,3));
                this.points.add(new Point(3,4));
                this.points.add(new Point(3,5));
                this.points.add(new Point(3,9));
                this.points.add(new Point(3,10));
                this.points.add(new Point(3,11));
                this.points.add(new Point(5,1));
                this.points.add(new Point(6,1));
                this.points.add(new Point(7,1));
                this.points.add(new Point(5,6));
                this.points.add(new Point(6,6));
                this.points.add(new Point(7,6));
                this.points.add(new Point(5,8));
                this.points.add(new Point(6,8));
                this.points.add(new Point(7,8));
                this.points.add(new Point(5,13));
                this.points.add(new Point(6,13));
                this.points.add(new Point(7,13));
                this.points.add(new Point(8,3));
                this.points.add(new Point(8,4));
                this.points.add(new Point(8,5));
                this.points.add(new Point(8,9));
                this.points.add(new Point(8,10));
                this.points.add(new Point(8,11));
                this.points.add(new Point(10,3));
                this.points.add(new Point(10,4));
                this.points.add(new Point(10,5));
                this.points.add(new Point(10,9));
                this.points.add(new Point(10,10));
                this.points.add(new Point(10,11));
                this.points.add(new Point(11,1));
                this.points.add(new Point(12,1));
                this.points.add(new Point(13,1));
                this.points.add(new Point(11,6));
                this.points.add(new Point(12,6));
                this.points.add(new Point(13,6));
                this.points.add(new Point(11,8));
                this.points.add(new Point(12,8));
                this.points.add(new Point(13,8));
                this.points.add(new Point(11,13));
                this.points.add(new Point(12,13));
                this.points.add(new Point(13,13));
                this.points.add(new Point(15,3));
                this.points.add(new Point(15,4));
                this.points.add(new Point(15,5));
                this.points.add(new Point(15,9));
                this.points.add(new Point(15,10));
                this.points.add(new Point(15,11));
                break;
            case 1: //Tetris
                this.points.add(new Point(2,3));
                this.points.add(new Point(3,3));
                this.points.add(new Point(4,3));
                this.points.add(new Point(5,3));

                this.points.add(new Point(3,5));
                this.points.add(new Point(3,6));
                this.points.add(new Point(4,6));
                this.points.add(new Point(3,7));

                this.points.add(new Point(3,9));
                this.points.add(new Point(3,10));
                this.points.add(new Point(4,9));
                this.points.add(new Point(4,10));

                this.points.add(new Point(8,3));
                this.points.add(new Point(8,4));
                this.points.add(new Point(8,5));
                this.points.add(new Point(9,5));

                this.points.add(new Point(9,9));
                this.points.add(new Point(8,7));
                this.points.add(new Point(8,8));
                this.points.add(new Point(9,8));
                break;
            case 2://pinwheel
                this.points.add(new Point(1,9));
                this.points.add(new Point(1,10));
                this.points.add(new Point(2,9));
                this.points.add(new Point(2,10));

                this.points.add(new Point(4,7));
                this.points.add(new Point(4,8));
                this.points.add(new Point(4,9));
                this.points.add(new Point(4,10));

                this.points.add(new Point(9,7));
                this.points.add(new Point(9,8));
                this.points.add(new Point(9,9));
                this.points.add(new Point(9,10));

                this.points.add(new Point(5,6));
                this.points.add(new Point(6,6));
                this.points.add(new Point(7,6));
                this.points.add(new Point(8,6));

                this.points.add(new Point(5,11));
                this.points.add(new Point(6,11));
                this.points.add(new Point(7,11));
                this.points.add(new Point(8,11));

                this.points.add(new Point(11,7));
                this.points.add(new Point(11,8));
                this.points.add(new Point(12,7));
                this.points.add(new Point(12,8));

                this.points.add(new Point(5,3));
                this.points.add(new Point(5,4));
                this.points.add(new Point(6,3));
                this.points.add(new Point(6,4));

                this.points.add(new Point(5,13));
                this.points.add(new Point(5,14));
                this.points.add(new Point(6,13));
                this.points.add(new Point(6,14));

                this.points.add(new Point(8,8));
                this.points.add(new Point(6,9));
                this.points.add(new Point(7,10));




                break;
            case 3:
                this.points.add(new Point(3,3));
                this.points.add(new Point(3,4));
                this.points.add(new Point(3,5));
                break;
            case 4:
                this.points.add(new Point(3,3));
                this.points.add(new Point(3,4));
                this.points.add(new Point(3,5));
                break;
            default:
                    break;

        }
    }
}
