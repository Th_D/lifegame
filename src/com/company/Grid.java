package com.company;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class Grid {
    private int height;
    private int lenght;

    private Gui gui;
    private ArrayList<ArrayList<Status>> status;

    private ChronoThread chrono;

    enum Status {alive, dead}

    /*
    CONSTRUCTORS
     */
    Grid(int h, int l) {
        height = h;
        lenght = l;
        status = new ArrayList<>();
        chrono = new ChronoThread(this);
        chrono.start();
        gui = new Gui(h, l);
        for (int i = 0; i < l; i++) {
            status.add(new ArrayList<>());
            for (int j = 0; j < h; j++)
                status.get(i).add(Status.dead);
        }
        connectGUI();
    }

    /*
    OVERRIDE (util)
     */
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < lenght; j++) {
                str.append(this.status.get(j).get(i)).append(" ");
            }
            str.append("\n");
        }
        return str.toString();
    }

    /*
    PRIVATE METHODS
     */
    private Status get(int h, int l) {
        return this.status.get(l).get(h);
    }

    private void set(int h, int l, Status s) {
        this.status.get(l).set(h, s);
        gui.setStatus(h, l, s);
    }

    private void connectGUI() {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < lenght; j++) {
                JButton btn = gui.getButton(i, j);
                int ii = i;
                int jj = j;
                btn.addActionListener(e -> {
                    if (get(ii, jj) == Status.dead) {
                        System.out.println("1");
                        set(ii, jj, Status.alive);
                    } else if (get(ii, jj) == Status.alive) {
                        System.out.println("2");
                        set(ii, jj, Status.dead);
                    }
                });
            }
        }
        gui.btnNext.addActionListener(e -> performStep());
        gui.btnStart.addActionListener(e -> {
            chrono.changeState();
            gui.startPause();
        });

        gui.btnRestart.addActionListener(e -> clear());

        gui.cbFigures.addActionListener(e -> {
            clear();
            Figure figure = new Figure(gui.cbFigures.getSelectedIndex());
            this.setFigure(figure);
        });
    }

    private void setFigure(Figure figure) {
        for(Point p : figure.points){
            set(p.p1, p.p2, Status.alive);
        }
    }

    private ArrayList<Status> getVoisins(ArrayList<ArrayList<Status>> sts, int x, int y) {
        ArrayList<Status> ret = new ArrayList<>();
        if ((x - 1) >= 0 && (y) >= 0 && (x - 1) < lenght && (y) < height)
            ret.add(sts.get(x - 1).get(y));
        if ((x) >= 0 && (y-1) >= 0 && (x) < lenght && (y-1) < height)
            ret.add(sts.get(x).get(y-1));
        if ((x) >= 0 && (y + 1) >= 0 && (x) < lenght && (y + 1) < height)
            ret.add(sts.get(x).get(y + 1));
        if ((x + 1) >= 0 && (y) >= 0 && (x + 1) < lenght && (y) < height)
            ret.add(sts.get(x+1).get(y));
        if ((x-1) >= 0 && (y-1) >= 0 && (x-1) < lenght && (y-1) < height)
            ret.add(sts.get(x-1).get(y-1));
        if ((x + 1) >= 0 && (y - 1) >= 0 && (x + 1) < lenght && (y - 1) < height)
            ret.add(sts.get(x + 1).get(y - 1));
        if ((x + 1) >= 0 && (y+1) >= 0 && (x + 1) < lenght && (y+1) < height)
            ret.add(sts.get(x + 1).get(y+1));
        if ((x - 1) >= 0 && (y + 1) >= 0 && (x - 1) < lenght && (y + 1) < height)
            ret.add(sts.get(x - 1).get(y + 1));
        return ret;
    }

    private int cptAlive(ArrayList<Status> sts) {
        int ret = 0;
        for (Status s : sts) {
            if (s == Status.alive)
                ret++;
        }
        return ret;
    }

    /*
    PUBLIC METHODS
     */
    void performStep() {
        ArrayList<ArrayList<Status>> copy = new ArrayList();
        for (int i = 0; i < lenght; i++) {
            copy.add(new ArrayList<>());
            for (int j = 0; j < height; j++)
                copy.get(i).add(status.get(i).get(j));
        }
        for (int i = 0; i < lenght; i++) {
            for (int j = 0; j < height; j++)
                if (get(j, i) == Status.alive) {
                    if (cptAlive(getVoisins(copy, i, j)) < 2) {
                        System.out.println("Solitude" + i + " " + j);
                        set(j, i, Status.dead);
                    } else if (cptAlive(getVoisins(copy, i, j)) >= 4) {
                        System.out.println("Overpopulation" + i + " " + j);
                        set(j, i, Status.dead);
                    }
                } else if (get(j, i) == Status.dead) {
                    if (cptAlive(getVoisins(copy, i, j)) == 3) {
                        System.out.println("Birth" + i + " " + j);
                        set(j, i, Status.alive);
                    }
                }
        }
    }

    private void clear(){
        for (int i = 0; i < lenght; i++)
            for (int j = 0; j < height; j++)
                set(j, i, Status.dead);
    }
}
