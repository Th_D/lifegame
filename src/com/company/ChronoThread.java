package com.company;

public class ChronoThread extends Thread {
    private Grid grid;
    private boolean state;

    ChronoThread(Grid g){
        super();
        state = false;
        grid = g;
    }

    void changeState(){
        if(state)
            pause();
        else
            go();
    }

    private void go(){
        state = true;
    }

    private void pause(){
        state = false;
    }
    /*
    OW
     */
    public  void run() {
        while(true){
            if(state){
                //action to perform
                grid.performStep();
            }
            try {
                sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
