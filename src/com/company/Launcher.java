package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Launcher {
    private JFrame frame;

    private JButton btnLaunch;
    private JButton btnClose;

    private JLabel lblHead1;
    private JLabel lblHead2;
    private JLabel lblRow;
    private JLabel lblCol;

    private JTextField tfRow;
    private JTextField tfLenght;


    public Launcher(){
        fill();
        connect();
        frame.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
                System.exit(0);
            }
        });
    }

    private void fill(){
        frame =  new JFrame();
        frame.setVisible(true);
        frame.setLayout(new GridLayout(4,2));

        lblHead1 = new JLabel("LifeGame");
        lblHead2 = new JLabel("by ThD");
        lblRow = new JLabel("Row :");
        lblCol = new JLabel("Col :");
        tfRow = new JTextField("15");
        tfLenght = new JTextField("15");
        frame.add(lblHead1);
        frame.add(lblHead2);
        frame.add(lblRow);
        frame.add(tfRow);
        frame.add(lblCol);
        frame.add(tfLenght);

        btnClose = new JButton("Close");
        btnLaunch = new JButton("Create !");
        frame.add(btnClose);
        frame.add(btnLaunch);

        frame.pack();
        frame.setSize(frame.getWidth()+30, frame.getHeight()+30);
    }

    private void connect(){
        btnClose.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
            }
        });

        btnLaunch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int l = -1;
                int c = -1;

                try {
                    l = Integer.parseInt(tfLenght.getText());
                    // is integer
                    if(l>0 && l<50)
                        tfLenght.setBackground(Color.white);
                    else{
                        l = -1;
                        tfLenght.setBackground(Color.red);
                    }
                } catch(NumberFormatException ex){
                    // not an integer
                    System.out.println("not integer");
                    tfLenght.setBackground(Color.RED);
                }

                try {
                    c = Integer.parseInt(tfRow.getText());
                    // is integer
                    if(c>0 && c<50)
                        tfRow.setBackground(Color.white);
                    else{
                        c = -1;
                        tfRow.setBackground(Color.red);
                    }
                } catch(NumberFormatException ex){
                    // not an integer
                    System.out.println("not integer");
                    tfRow.setBackground(Color.RED);
                }
                if(c!=-1 && l!=-1){
                    Grid grid = new Grid(c, l);
                }
            }
        });
    }

}
