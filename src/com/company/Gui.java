package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

class Gui {
    private JFrame frame;
    private GridLayout grid;
    private ArrayList <JButton> buttons;
    JButton btnStart;
    JButton btnRestart;
    JButton btnNext;
    Object[] figures = new Object[]{"Pulsar", "Tetromino", "Pinwheel", "Disco", "Magic stick"};
    // Pulsar : http://conwaylife.com/w/index.php?title=Pulsar
    // Tetromino : tetromino
    // Pinwheel : http://conwaylife.com/wiki/Pinwheel
    // Disco : http://conwaylife.com/wiki/Circle_of_fire
    // Magic stick : http://conwaylife.com/wiki/Figure_eight
    JComboBox cbFigures;

    private int lenght;
    private int height;

    /*
    CONSTRUCTORS
     */
    Gui(int h, int l){
        lenght = l;
        height = h;
        frame =  new JFrame();
        frame.setVisible(true);
        grid = new GridLayout(h+1,l);
        frame.setLayout(grid);
        buttons = new ArrayList();

        for(int i = 0; i< (height*lenght); i++) {
            JButton btn = new JButton();
            frame.add(btn);
            btn.setBackground(Color.GRAY);
            buttons.add(btn);
        }

        btnNext = new JButton("Next");
        btnStart = new JButton("Start"); //switch to pause when clicked
        btnRestart = new JButton("Clear");
        cbFigures = new JComboBox(figures);

        frame.add(btnNext);
        frame.add(btnStart);
        frame.add(btnRestart);
        frame.add(cbFigures);

        frame.pack();

        frame.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
                System.exit(0);
            }
        });


    }

    /*
    PRIVATE METHODS
     */


    /*
    PUBLIC METHODS
     */
    void setStatus(int x, int y, Grid.Status s){
        if (s == Grid.Status.dead){
            buttons.get(lenght*x+y).setBackground(Color.GRAY);
        } else if(s == Grid.Status.alive){
            buttons.get(lenght*x+y).setBackground(Color.YELLOW);
        }
    }

    JButton getButton(int x, int y){
        System.out.println(buttons.size()+" getButton ["+x+" "+y+"] -> "+lenght*x+y);
        return buttons.get((lenght*x)+y);
    }

    void startPause(){
        if (btnStart.getText().equals("Start"))
            btnStart.setText("Pause");
        else
            btnStart.setText("Start");
    }

    void drawFigure(Figure f){
        for (Point p : f.points){
            this.drawPoint(p);
        }
    }

    void drawPoint(Point p){
        setStatus(p.p1, p.p2, Grid.Status.alive);
    }
}
